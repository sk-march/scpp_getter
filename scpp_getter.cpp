#include <scriptive_cpp.h>
#include <iostream>
#include <cppcmd.h>
using namespace std;

jit_export void learn(const char* file_name) {
	cout << cppcmd::pwd() << endl;
	cout << "learn getter in " << file_name << endl;
}

jit_export void forget(const char* file_name) {
	cout << "forget getter in " << file_name << endl;
}